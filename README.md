# C Programming Language Course 

C programlama dili kursu notu CSD - C ve Sistem Programcıları Derneği tarafından  sağlanmıştır. Kursun anlatıcısı Kaan Aslan'dır.

[CSD C notlarının](https://github.com/CSD-1993/KursNotlari) tamamını kapsaması hedeflenmektedir. Bu depodaki içerikler Sistem Programlama ve UNIX/Linux(kernel programlama) derslerini de içerir şekilde yazılacaktır.

## Ders Oynatma Listesi

[2025 C Dersleri Serisi ](https://www.youtube.com/watch?v=I3A_TyeCjAA&list=PLXskldhH5VMpyiR8ZDC7I8a-C_3XfclkC)

## License

Kaan Aslan'ın notundan faydalanılmıştır.
