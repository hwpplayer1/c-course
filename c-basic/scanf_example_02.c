#include <stdio.h>

int main()
{
  int a;
  int b;

  printf("Enter number a:\n");
  scanf("%d", &a);
  printf("Enter number b:\n");
  scanf("%d", &b);
  printf("a = %d, b = %d\n", a, b);

  return 0;
}
